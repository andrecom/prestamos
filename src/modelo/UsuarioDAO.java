package modelo;

import java.sql.SQLException;
import java.sql.Statement;

public class UsuarioDAO extends DB implements DAO {
	
	public void create(Usuario usuario) {
		Statement stmt;
		try {
				stmt = this.getStatement();
				stmt.executeUpdate("INSERT INTO Usuario"
						+ "(rut,"
						+ "nombre,"
						+ "apellido,"
						+ "correo,"
						+ "tipo,"
						+ "calle,"
						+ "numerocasa,"
						+ "reputacion_id,"
						+ "comuna_id)"
						+ "VALUES("
						+"'"+usuario.getRut()+"',"
						+"'"+usuario.getNombre()+"',"
						+"'"+usuario.getApellido()+"',"
						+"'"+usuario.getCorreo()+"',"
						+"'"+usuario.getTipo()+"',"
						+"'"+usuario.getCalle()+"',"
						+"'"+usuario.getNumeroCasa()+"',"
						+usuario.getReputacionId()+"'"
						+usuario.getComunaId()+")");
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	@Override
	public int update(int id, String campo,String valor) {
		try {
				Statement unStmt = this.getStatement();
				unStmt.executeUpdate("UPDATE Usuario SET"
						+ campo+"="+valor);
		} catch (SQLException e) {
				System.out.println(e.getMessage().toString());
		}
		
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object get(int cantidad) {
		// TODO Auto-generated method stub
		return null;
	}

}
