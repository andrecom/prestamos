package modelo;

public class Usuario {
	private String rut;
	private String nombre;
	private String apellido;
	private String correo;
	private String tipo;
	private String calle;
	private String numeroCasa;
	private String reputacionId;
	private String comunaId;	
	
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}	
	public String getNumeroCasa() {
		return numeroCasa;
	}
	public void setNumeroCasa(String numeroCasa) {
		this.numeroCasa = numeroCasa;
	}
	public String getReputacionId() {
		return reputacionId;
	}
	public void setReputacionId(String reputacionId) {
		this.reputacionId = reputacionId;
	}
	public String getComunaId() {
		return comunaId;
	}
	public void setComunaId(String comunaId) {
		this.comunaId = comunaId;
	}
}
